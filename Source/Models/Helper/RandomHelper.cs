﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sicarii.Core.Helper
{
	public class RandomHelper
	{
		private static Random rand;

		static RandomHelper()
		{
			int seed = (int)DateTime.Now.Ticks;

			rand = new Random((int)seed);
		}

		public static void SetSeed(int seed)
		{
			rand = new Random(seed);
		}

		public static OpenTK.Vector3 GetRandomPosition(float min = -100, float max = 100)
		{
			return new OpenTK.Vector3()
			{
				X = (int)GetRandomFloat(min,max),
				Y = (int)GetRandomFloat(min, max),
				Z = (int)GetRandomFloat(min, max)
			};
		}

		public static string GetRandomName()
		{
			return Resources.Name.Names[rand.Next(Resources.Name.Names.Length)];
		}

		public static float GetRandomFloat(float min, float max)
        {
            return (float)rand.NextDouble() * (max - min) + min;
        }

	}
}
