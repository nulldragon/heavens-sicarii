﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sicarii.Core.Resources
{
	public static class Name
	{
		public static string[] Names = new string[]
		{
			"Shirlene",			"Ryan",			"Evette",			"Kirk",			"Tod",			"Jeraldine",			"Deandre",			"Karolyn",			"Antonina",			"Berenice",			"Yasmin",			"Terrence",			"Leida",			"Shantae",			"Florentino",			"Debbi",			"Rubie",			"Hilary",			"Lyndia",			"Rachelle",			"Annemarie",			"Odelia",			"Virgil",			"Tu",			"Marianela",			"Jennefer",			"Monroe",			"Janina",			"Alvera",			"Elena",			"Shizuko",			"Deanne",			"Birdie",			"Aurore",			"Tasha",			"Terry",			"Jarrod",			"Devora",			"Estell",			"Kelsey",			"Johnathan",			"Maxie",			"Bobette",			"Faith",			"Aletha",			"Jonathon",			"Otto",			"Margene",			"Oren",			"Erma"
		};
	}
}
