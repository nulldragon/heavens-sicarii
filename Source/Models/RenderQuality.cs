﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sicarii.Core
{
	public enum RenderQuality
	{
		Low,
		Medium,
		High,
		Extreme,
	}
}
