﻿using Lidgren.Network;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sicarii.Core.World
{
	/*
	 * Design Notes:
	 * I have opted to avoid having planets in solar systems as very little is gained from the added complexity
	 * See SWOTS2 or several other 4X games
	 */

	/// <summary>
	/// A Star in the Galaxy
	/// Provides player with a place to interact with
	/// </summary>
    public class Star
    {
		/// <summary>
		/// Unique Planet Id
		/// Generated externally
		/// Caution: Do Not Duplicate Ids
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Position in Galaxy
		/// </summary>
		public Vector3 Position { get; set; }

		/// <summary>
		/// Solar Body's Name
		/// </summary>
		public string Name { get; set; }
						
		/// <summary>
		/// Impliment a simple environmental hazard factor
		/// Between -1000 and 1000
		/// with 0 Being optimal
		/// </summary>
		public int EnvironmentalHazard { get { return environmentalHazard; } set { environmentalHazard = value > 1000 ? 1000 : value < -1000 ? -1000 : value; } }
		private int environmentalHazard;

		public Star()
		{
			Id = -1;
			Position = Vector3.Zero;
			Name = "Star";
			environmentalHazard = -9999;
		}

		public Star(Lidgren.Network.NetIncomingMessage msg)
		{
			msg.ReadAllProperties(this);
		}

		/// <summary>
		/// Get the distance between two stars
		/// </summary>
		/// <param name="source">Starting star</param>
		/// <param name="target">Ending star</param>
		/// <returns>Distance</returns>
		public static float Distance(Star source, Star target)
		{
			var deltaX = (target.Position.X - source.Position.X);
			var deltaY = (target.Position.Y - source.Position.Y);
			var deltaZ = (target.Position.Z - source.Position.Z);

			float distance = (float)Math.Sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);

			return distance;
		}

		/// <summary>
		/// Get the distance between this star and another
		/// </summary>
		/// <param name="target">Target star</param>
		/// <returns>Distance</returns>
		public float Distance(Star target)
		{
			return Distance(this, target);
		}

		public static bool PositionOk(int minStarDistance, Star star, ref Map newMap)
		{
			int toClose = (from s in newMap.Stars
						   where s.Distance(star) < minStarDistance
						   select s).Count();

			if (toClose > 0)
				return false;

			return true;
		}

		public Lidgren.Network.NetOutgoingMessage BuildMessage(ref NetPeer peer)
		{
			NetOutgoingMessage msg = peer.CreateMessage();
			msg.Write((int)Sicarii.Core.Network.MessageType.TransferPlanet);
			msg.WriteAllProperties(this);
			return msg;
		}		
	}
}
