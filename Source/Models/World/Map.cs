﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sicarii.Core.World
{
	public class Map
	{
		public List<Star> Stars { get; set; }

		public Map()
		{
			Stars = new List<Star>();
		}

		public void Transmit(NetPeer peer, NetConnection rcpt)
		{
			foreach (var star in Stars)
				peer.SendMessage(star.BuildMessage(ref peer), rcpt, NetDeliveryMethod.ReliableOrdered);
		}

		/// <summary>
		/// Generates a TOTALLY random map
		/// </summary>
		/// <param name="starCount"></param>
		/// <param name="minStarDistance"></param>
		/// <returns></returns>
		public static Map GenerateRandom(int starCount = 100, int minStarDistance = 6)
		{
			Map newMap = new Map();

			for (int i = 0; i <= starCount - 1; i++)
			{
				bool success = false;
				while (!success)
				{
					Star star = new Star()
					{
						Position = Sicarii.Core.Helper.RandomHelper.GetRandomPosition(),
						Name = Sicarii.Core.Helper.RandomHelper.GetRandomName()
					};
					
					if (Star.PositionOk(minStarDistance, star, ref newMap))
					{
						star.Id = i;
						newMap.Stars.Add(star);
						success = true;
					}
				}
			}

			return newMap;
		}
	}
}
