﻿using System;
using System.Collections.Generic;

namespace Sicarii.Core
{
	public class Configuration
	{
		public int ServerPort { get; set; }
		
		/// <summary>
		/// Heavens Dagger
		/// </summary>
		public string GameName { get { return "Heaven's Sicarii"; } }

		public Resolution DisplayResolution { get; set; }

		public RenderQuality RenderQuality { get; set; }
	}
}
