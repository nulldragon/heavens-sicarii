﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sicarii.Core.Network
{
	public enum MessageType
	{
		/// <summary>
		/// Keep alive message - no content
		/// </summary>
		Heartbeat,
		/// <summary>
		/// Update games state
		/// </summary>
		GameState,
		/// <summary>
		/// Updates a players state
		/// </summary>
		PlayerState,
		/// <summary>
		/// Message from Client/Server -> turn complete etc
		/// </summary>
		GameMessage,
		/// <summary>
		/// Message to/from a player to another player/s
		/// </summary>
		PlayerMessage,
		/// <summary>
		/// Send a map
		/// </summary>
		TransferMap,
		/// <summary>
		/// Send a planet
		/// </summary>
		TransferPlanet,
	}
}
