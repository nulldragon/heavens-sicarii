﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sicarii.Core
{
	public static class Defaults
	{
		public static Configuration Configuration { get; private set; }


		static Defaults()
		{
			Configuration = new Configuration();

			Configuration.DisplayResolution = new Resolution() { Width = 800, Height = 600 };
			Configuration.RenderQuality = RenderQuality.Medium;
			Configuration.ServerPort = 10700;
		}
	}
}
