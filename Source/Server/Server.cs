﻿/*
 * Server
 * 
 * 
 * 
 */
using Lidgren.Network;
using Sicarii.Core;
using Sicarii.Core.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
	/// <summary>
	/// Instance of the Server 
	/// </summary>
	class Program
	{
		private static NetServer server;
		private static System.Threading.Thread serverThread;
		private static bool shutdown;

		static void Main(string[] args)
		{
			SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

			System.Console.WriteLine("Starting Server v" + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion);
			System.Console.WriteLine("Esc to exit");

			//Setup server with default configuration options
			SetupServer();

			//Run sever thread reader until the end of days
			serverThread = new Thread(() => Listen());
			shutdown = false;
			serverThread.Start();

			//End of Server Cleanup Follows this Wait
			WaitForEscapeAsync().Wait();
			
			//If we get here we are done
			shutdown = true;
			serverThread.Join();//wait until the server loop is completed and exits
			server.Shutdown("Server Terminated");

#if DEBUG 
			Console.WriteLine("\n\nFinished... Press any key to close");
			Console.ReadKey();
#endif
		}

		private static async Task WaitForEscapeAsync()
		{
			while (Console.ReadKey().Key != ConsoleKey.Escape)
			{
				await Task.Yield();
			}
		}

		private static void SetupServer()
		{
			NetPeerConfiguration config = new NetPeerConfiguration(Sicarii.Core.Defaults.Configuration.GameName);

			config.Port = Defaults.Configuration.ServerPort;
			//config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
			//config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

			server = new NetServer(config);
			server.Start();
			
			Console.WriteLine("Running..");
		}

		private static void Listen()
		{
			while (true)
			{
				if (shutdown)
					return;//finished stop halt

				NetIncomingMessage msg;
				while ((msg = server.ReadMessage()) != null)
				{
					switch (msg.MessageType)
					{
						case NetIncomingMessageType.DiscoveryRequest:
							Console.WriteLine("Todo Discovery Request Handling");
							break;
						case NetIncomingMessageType.ConnectionApproval:
							Console.WriteLine("Approved Connection");
							msg.SenderConnection.Approve();
							break;
						case NetIncomingMessageType.StatusChanged:
							Console.WriteLine("Status changed\t" + msg.ReadString() + "\t" + msg.SenderConnection.RemoteEndPoint.ToString());
							break;
						case NetIncomingMessageType.Data:
							ProcessData(msg);
							break;
						case NetIncomingMessageType.VerboseDebugMessage:
						case NetIncomingMessageType.DebugMessage:
						case NetIncomingMessageType.WarningMessage:
						case NetIncomingMessageType.ErrorMessage:
							Console.WriteLine(msg.ReadString());
							break;
						default:
							Console.WriteLine("Unhandled type: " + msg.MessageType);
							break;
					}
					server.Recycle(msg);
				}
				System.Threading.Thread.Yield();
			}//permaloop
		}

		private static void ProcessData(NetIncomingMessage msg)
		{
			var messageType = (MessageType)msg.ReadInt32();

			switch(messageType)
			{
				case MessageType.Heartbeat:
					Console.WriteLine("Beat " + msg.SenderConnection.RemoteEndPoint.ToString());
					break;
				case MessageType.TransferPlanet:
					var s = new Sicarii.Core.World.Star(msg);
					Console.WriteLine(s.Id + "\t" + s.Name);
					break;
				default:
					Console.WriteLine("Unhandled sicarii message type: " + messageType + "\t" + msg.LengthBytes);
					break;
			}
		}
	}
}
