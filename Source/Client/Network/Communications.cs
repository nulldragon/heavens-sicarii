﻿using Lidgren.Network;
using Sicarii.Core.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client.Network
{
	public class Communications
	{
		private bool shutdown;
		private Thread commsThread;

		private NetClient client;
		
		public event NewMessageHandler NewMessage;
		public delegate void NewMessageHandler(object sender, Network.MessageEventArgs e);

		public Communications(ref NetClient client)
		{
			this.client = client;
		}

		public void Start()
		{
			shutdown = false;
			commsThread = new Thread(() => Process());
			commsThread.Start();
		}

		public void Stop()
		{
			shutdown = true;
		}

		public void Process()
		{
			while (!shutdown)
			{
				NetIncomingMessage msg;
				while ((msg = client.ReadMessage()) != null)
				{
					switch (msg.MessageType)
					{
						case NetIncomingMessageType.Data:
							ProcessData(msg);
							break;
						default:
							break;
					}
					client.Recycle(msg);
				}

				System.Threading.Thread.Yield();
			}
		}

		//todo
		public void SendMessage()
		{
			//test message
			NetOutgoingMessage sendMsg = client.CreateMessage();
			sendMsg.Write("test");
			client.SendMessage(sendMsg, NetDeliveryMethod.Unreliable);

		}

		private void ProcessData(NetIncomingMessage msg)
		{
			var messageType = (MessageType)msg.ReadInt32();

			switch(messageType)
			{
				case MessageType.TransferMap:
					//Sicarii.Core.World.Map map = Sicarii.Core.Helper.Serializer.ByteArrayToObject(msg.)
					break;
				default:
					break;
			}

			var e = new Network.MessageEventArgs();
			e.Data = msg.Data;
			e.MessageType = messageType;
			OnNewMessage(this, e);
		}

		#region Events
		
		protected virtual void OnNewMessage(object sender, Network.MessageEventArgs e)
		{
			if (NewMessage != null)
				NewMessage(this, e);
		}

		#endregion
	}
}
