﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Network
{
	public class MessageEventArgs : EventArgs
	{
		public Sicarii.Core.Network.MessageType MessageType { get; set; }
		public byte[] Data { get; set; }
	}
}
