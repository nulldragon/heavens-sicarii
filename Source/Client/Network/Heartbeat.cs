﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client.Network
{
	public class Heartbeat
	{
		private bool shutdown;
		private Thread beatThread;

		private NetClient client;

		private const int beatMillis = 1500;

		public Heartbeat(ref NetClient client)
		{
			this.shutdown = false;
			this.client = client;
		}

		public void Start()
		{
			shutdown = false;
			beatThread = new Thread(() => Beat());
			beatThread.Start();
		}

		public void Stop()
		{
			shutdown = true;
		}

		private void Beat()
		{
			while(!shutdown)
			{
				var msg = client.CreateMessage();
				msg.Write((int)Sicarii.Core.Network.MessageType.Heartbeat);

				client.SendMessage(msg, NetDeliveryMethod.Unreliable);//don't want retries or anything fancy just hit the server with a message

				Thread.Sleep(beatMillis);
			}
		}
	}
}
