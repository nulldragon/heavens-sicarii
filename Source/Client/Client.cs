﻿/*
 * Client
 * 
 * 
 */
using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
	class Program
	{
		private static NetClient client;

		private static Network.Heartbeat keepAlive;
		private static Network.Communications comms;
		private static Render.GameDisplay gameDisplay;

		static void Main(string[] args)
		{
			SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

			SetupClient();

			#region Debuging test Code

			foreach (OpenTK.DisplayIndex index in Enum.GetValues(typeof(OpenTK.DisplayIndex)))
			{
				OpenTK.DisplayDevice device = OpenTK.DisplayDevice.GetDisplay(index);
				if (device == null)
				{
					continue;
				}

				Console.WriteLine("Display\t" + device);
				//Console.WriteLine(device.Bounds);
				//Console.WriteLine(device.RefreshRate);
				//Console.WriteLine(device.BitsPerPixel);

				/*foreach (OpenTK.DisplayResolution res in device.AvailableResolutions)
				{
					Console.WriteLine(res);
				}*/
			}

			//Todo ASK server for the map
			//Build a map 
			Sicarii.Core.World.Map galaxy = Sicarii.Core.World.Map.GenerateRandom();
			Console.WriteLine("Star Count: " + galaxy.Stars.Count);
			foreach(var s in galaxy.Stars)
			{
				Console.WriteLine(s.Name + "[" + s.Id + "]\t" + s.Position);
			}

			galaxy.Transmit(client, client.ServerConnection);

			#endregion

			//Setup GL

			//gameDisplay = new Render.GameDisplay();
			//gameDisplay.Setup();
			//gameDisplay.Start();//display the render window

			WaitForEscapeAsync().Wait();

			//clean up kill threads
			keepAlive.Stop();
			comms.Stop();
		}

		//Wait for an escape keypress to kill the console
		//GL doesnt pass through any input so this only halts the console from closing by itself
		private static async Task WaitForEscapeAsync()
		{
			ConsoleKey key;
			while ((key = Console.ReadKey().Key) != ConsoleKey.Escape)
			{
				Send(key.ToString());

				await Task.Yield();
			}

			keepAlive.Stop();
			client.Shutdown("Closing connection.");
		}

		#region Client Networking
		//Setup networking client
		private static void SetupClient()
		{
			//Test Network library
			NetPeerConfiguration config = new NetPeerConfiguration(Sicarii.Core.Defaults.Configuration.GameName);			
			
			client = new NetClient(config);
			client.Start();

			//Connection Apporval

			client.Connect("192.168.16.126", Sicarii.Core.Defaults.Configuration.ServerPort);

			System.Threading.Thread.Sleep(1000);

			//setup keep alive
			keepAlive = new Network.Heartbeat(ref client);
			keepAlive.Start();

			comms = new Network.Communications(ref client);
			comms.NewMessage += comms_NewMessage;
			comms.Start();			
		}

		static void comms_NewMessage(object sender, Network.MessageEventArgs e)
		{
			Console.WriteLine(e.MessageType + "\t" + e.Data.Length);
		}

		private static void Send(string message)
		{
			NetOutgoingMessage sendMsg = client.CreateMessage();
			sendMsg.Write(sendMsg);
			client.SendMessage(sendMsg, NetDeliveryMethod.ReliableOrdered);
		}

		private static void ProcessMessage(object sender)
		{
			//NetServer server = peer as NetServer;
			NetPeer peer = sender as NetPeer;
			NetIncomingMessage message = peer.ReadMessage();

			switch (message.MessageType)
			{
				case NetIncomingMessageType.DiscoveryResponse:
					Console.WriteLine("Discovery Response");
					peer.Connect(message.SenderEndPoint);
					Console.WriteLine("Connected");
					break;
				case NetIncomingMessageType.VerboseDebugMessage:
				case NetIncomingMessageType.DebugMessage:
				case NetIncomingMessageType.WarningMessage:
				case NetIncomingMessageType.ErrorMessage:
					Console.WriteLine(message.ReadString());
					break;
				default:
					Console.WriteLine("Unhandled Type: " + message.MessageType + "\t" + message.ReadString());
					break;
			}

			peer.Recycle(message);
		}
		#endregion
				
	}
}
