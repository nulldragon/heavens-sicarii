﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Render
{
	public class GameDisplay
	{
		private GameWindow window;

		public GameDisplay()
		{

		}

		~GameDisplay()
		{
			if (window != null)
				window.Dispose();
			window = null;
		}

		public void Setup()
		{
			window = new GameWindow();

			window.Load += Load;
			window.Resize += Resize;
			window.UpdateFrame += UpdateFrame;
			window.RenderFrame += RenderFrame;

			//input handling
			//window.KeyPress += window_KeyPress;//char only?
			window.KeyDown += GameKeyDown;

			window.Title = Sicarii.Core.Defaults.Configuration.GameName;
			

		}

		public void Start()
		{
			if (window == null)
				throw new Exception("GameDisplay.Setup not called first");

			window.Run(60,60);
		}

		#region Keyboard & Mouse
		void GameKeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
		{
			if (e.Key == OpenTK.Input.Key.Escape)
				window.Exit();
		}
		#endregion

		private void Load(object sender, EventArgs e)
		{
			window.TargetRenderFrequency = 60;
			window.VSync = VSyncMode.Adaptive;

			// setup settings, load textures, sounds
		}

		private void Resize(object sender, EventArgs e)
		{
			GL.Viewport(0, 0, window.Width, window.Height);
		}

		private void UpdateFrame(object sender, EventArgs e)
		{
			//game cycle update
		}

		private void RenderFrame(object sender, EventArgs e)
		{
			// render graphics
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			
			

			window.SwapBuffers();
		}
	}
}
